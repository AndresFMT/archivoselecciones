  
<div class="camara">

  <div class="header">

    <div class="generalInfo">

      <div class="left">

        <!-- SELECT OPTION -->
        <select class="dptSelect" (change)="selectCamDpto($event)">
          <option>SELECCIONE DEPARTAMENTO</option>
          <option *ngFor="let item of partidosCamaraData; let i = index" [value]="i" >
            {{item.departamento}} 
          </option>
        </select>

        <!-- TITLE DPTO -->
        <h3 class="dptoTitle">{{lPartidosCamaraByDept.departamento}}</h3>

      </div>

      <div class="right" [ngSwitch]="seccionCam">

        <div class="info-block" *ngSwitchCase="'partCamSecc'">

          <div class="block block-a">
            <div class="top">
              <div class="release">
                <b class="releaseText"> Boletín {{lPartidosCamaraByDept.numero}}</b>
                <span class="releaseTime">{{lPartidosCamaraByDept.hora}}</span>
              </div>
              <div class="info">
                  <span class="infoText">Informado</span>
                  <b class="infoPercent"> {{lPartidosCamaraByDept.porcentaje_mesas_informadas}}</b>
                </div>
            </div>

            <div class="bottom">
              <span class="curTitle">Curules en disputa</span> <b class="curNumber">10</b>
            </div>

          </div>

          <div class="block block-b">

            <div class="totalBlock">
              <span class="title"> Total de <br/> mesas informadas </span> 
              <b class="resault"> {{lPartidosCamaraByDept.mesas_informadas}} </b> 
            </div>
            
            <div class="totalBlock">
                <span class="title"> Total de <br/> mesas instaladas </span> 
                <b class="resault"> {{lPartidosCamaraByDept.mesas_instaladas}} </b> 
            </div>
          </div>

        </div>

        <div class="info-block" *ngSwitchCase="'candCamSecc'">
          <div class="block block-a">
            <div class="top">
              <div class="release">
                <b class="releaseText"> Boletín {{lCandidatosCamaraByDept.numero}}</b>
                <span class="releaseTime">{{lCandidatosCamaraByDept.hora}}</span>
              </div>
              <div class="info">
                  <span class="infoText">Informado</span>
                  <b class="infoPercent"> {{lCandidatosCamaraByDept.porcentaje_mesas_informadas}}</b>
                </div>
            </div>

            <div class="bottom">
              <span class="curTitle">Curules en disputa</span> <b class="curNumber">10</b>
            </div>

          </div>

          <div class="block block-b">
            <div class="totalBlock">
              <span class="title"> Total de <br/> mesas informadas </span> 
              <b class="resault"> {{lCandidatosCamaraByDept.mesas_informadas}} </b> 
            </div>
            <div class="totalBlock">
                <span class="title"> Total de <br/> mesas instaladas </span> 
                <b class="resault"> {{lCandidatosCamaraByDept.mesas_instaladas}} </b> 
            </div>
          </div>
        </div>

          
      </div>

    </div>

    <!-- TABS -->

    <ul class="tabs">
      <li (click)="seccionCam = 'partCamSecc'" [ngClass]="{'active' : seccionCam == 'partCamSecc' }">PARTIDOS</li>
      <li (click)="seccionCam = 'candCamSecc'" [ngClass]="{'active' : seccionCam == 'candCamSecc' }">CANDIDATOS</li>
    </ul>
  
  </div>

  <!-- TAB PANE CONTAINER -->

  <div class="tab-sections" [ngSwitch]="seccionCam">

    <!-- PARTIDOS -->

    <div class="tab-section" *ngSwitchCase="'partCamSecc'">

      <!-- DEPARTAMENTAL -->

      <div *ngFor="let group of lPartidosCamaraByDept.data; let g = index" > 

          <div class="group" *ngIf="lPartidosCamaraByDept.data[g].info.length">

            <div class="group-name">{{group.corp}}</div>

            <div class="item-wrap" *ngFor="let partido of group.info; let i = index" [ngClass]="{'widthAvatar': lPartidosCamaraByDept.data[g] == lPartidosCamaraByDept.data[0]}">
                <div class="imgAvatar" *ngIf="lPartidosCamaraByDept.data[g] == lPartidosCamaraByDept.data[0] && i < 9">
                  <img src="assets/img/partidos/{{partido.partidosNombrePartido | partido}}" />
                </div>
                <div class="item">
                  <p class="nombre"><b>{{i + 1}}. </b>{{partido.partidosNombrePartido}}</p>
                  <p class="votos">Votos: <b>{{partido.DetallePartidoVotos}}</b></p>
                  <div class="percentage">
                    <b class="pct">{{partido.DetallePartidoPorcentaje}} %</b>
                    <label class="progressbar">
                        <span [style.width.%]="partido.DetallePartidoPorcentaje"></span>
                      </label>
                  </div>
                </div>
            </div>

            <div class="filter" >
                <button class="filter-btn" >tomalo</button>
            </div>

        </div>

      </div>









    </div>

    <!-- CANDIDATOS -->

    <div class="candidatos-wrap tab-section" *ngSwitchCase="'candCamSecc'">

      <div *ngFor="let group of lCandidatosCamaraByDept.data; let g = index" >

          <div class="group" *ngIf="lCandidatosCamaraByDept.data[g].info.length">

            <div class="group-name">{{group.corp}}</div>

            <div class="item-wrap" *ngFor="let candidato of group.info; let i = index" [ngClass]="{'widthAvatar': lCandidatosCamaraByDept.data[g] == lCandidatosCamaraByDept.data[0]}">
                <div class="imgAvatar" *ngIf="lCandidatosCamaraByDept.data[g] == lCandidatosCamaraByDept.data[0] && i < 9">
                  <img src="assets/img/candidatos/{{candidato.partidosNombrePartido | partido}}" />
                </div>
                <div class="item">
                  <p class="nombre"><b>{{i + 1}}. </b>{{candidato.candidatosNombre}} </p>
                  <p class="pNombre"> {{candidato.partidosNombrePartido}}</p>
                  <p class="votos">Votos: <b>{{candidato.DetalleCandidatoVotos}}</b></p>
                  <div class="percentage">
                      <b class="pct">{{candidato.DetalleCandidatoPorcentaje}} %</b>
                      <label class="progressbar">
                          <span [style.width.%]="candidato.DetalleCandidatoPorcentaje"></span>
                        </label>
                    </div>
                </div>
            </div>

          </div>

      </div>

    </div>
  </div>

</div>