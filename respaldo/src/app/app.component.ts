import { Component, OnInit, SimpleChange } from '@angular/core';
import { DataVotosService } from './services/data-votos.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})

export class AppComponent implements OnInit{

  seccionA:string ='senSecc'
  sendDataSenado = {};
  sendDataCamara = {};
  sendDataGCC = {};
  sendDataCIS = {};
  scrollToTop:boolean = false;
  
  constructor(private listadosVotaciones:DataVotosService){ }

  ngOnInit(){

    var votosServices:any = () => {

      this.listadosVotaciones.getService('senado_nacional.json?' + Math.random()).subscribe(
        (data)=> {
          this.sendDataSenado = data;
      });
  
      this.listadosVotaciones.getService('camara_departamental.json?' + + Math.random()).subscribe(
        (data)=> {
          this.sendDataCamara = data;
      });

      this.listadosVotaciones.getService('archivoCIS.json?' + + Math.random()).subscribe(
        (data)=> {
          this.sendDataCIS = data;
      });

      this.listadosVotaciones.getService('archivoGCC.json?' + + Math.random()).subscribe(
        (data)=> {
          this.sendDataGCC = data;
      });

    }

    votosServices();

    // CALL THE SERVICES EVERY MINUTE 

    setInterval(() => { 

      votosServices();

    }, 60000);

    // DETECT SCROLL

    window.onscroll = () => {  
      if(window.scrollY > 130){
        this.scrollToTop = true;
      }else{
        this.scrollToTop = false;
      }
    }

  }

  // BUTTON SCROLL TOP

  toTop(scrollDuration) {
      var scrollStep = -window.scrollY / (scrollDuration / 15),

      scrollInterval = setInterval(function(){
          if ( window.scrollY != 0 ) {
              window.scrollBy( 0, scrollStep );
          }
          else clearInterval(scrollInterval); 
      },7);
  }

}
