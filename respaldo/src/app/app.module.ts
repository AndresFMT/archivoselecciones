import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule }  from '@angular/http'; 
import { DataVotosService } from './services/data-votos.service';

import { AppComponent } from './app.component';
import { PartidoPipe } from './pipes/partido.pipe';
import { CamaraComponent } from './components/camara/camara.component';
import { SenadoComponent } from './components/senado/senado.component';
import { ToNumberPipe } from './pipes/to-number.pipe';
import { CurulesPipe } from './pipes/curules.pipe';
import { ConsultasComponent } from './components/consultas/consultas.component';
import { CandconsultaPipe } from './pipes/candconsulta.pipe';


@NgModule({
  declarations: [
    AppComponent,
    PartidoPipe,
    CamaraComponent,
    SenadoComponent,
    ToNumberPipe,
    CurulesPipe,
    ConsultasComponent,
    CandconsultaPipe
  ],
  imports: [
    BrowserModule, HttpModule
  ],
  providers: [DataVotosService],
  bootstrap: [AppComponent]
})
export class AppModule { }
