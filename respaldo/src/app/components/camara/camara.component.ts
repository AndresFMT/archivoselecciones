import { Component, Input, SimpleChange } from '@angular/core';
// import { VotacionesCamModel } from '../../models/camara/votacionesCam.model';
import { PartidosCamModel } from '../../models/camara/partidos/partidosCam.model';
import { CandidatosCamModel } from '../../models/camara/candidatos/candidatosCam.model';


@Component({
  selector: 'app-camara',
  templateUrl: './camara.component.html',
  styleUrls: ['./camara.component.sass']
})
export class CamaraComponent {
  @Input() dataCamara;

  // lPartidosCamaraByDept:PartidosCamModel[]=[];

  lPartidosCamaraByDept:PartidosCamModel;
  lCandidatosCamaraByDept:CandidatosCamModel;
  deptoValue:number = 9;
  partidosCamaraData:PartidosCamModel;
  candidatosCamaraData:CandidatosCamModel;
  seccionCam:string ='candCamSecc'; 
  print:boolean = false;  
  addPartDepItems:number = 9;
  addPartIndItems:number = 6;
  addPartAfrItems:number = 6;

  addCandDepItems:number = 9;
  addCandIndItems:number = 6;
  addCandAfrItems:number = 6;


  constructor() {}

  ngOnChanges(changes:SimpleChange){

    if(Object.keys(this.dataCamara).length){
      
      this.lPartidosCamaraByDept = this.dataCamara.partidos[this.deptoValue];
      this.lCandidatosCamaraByDept = this.dataCamara.candidatos[this.deptoValue];
      this.partidosCamaraData = this.dataCamara.partidos;
      this.candidatosCamaraData = this.dataCamara.candidatos;

      this.print = true; 

    }

  }

  selectCamDpto(event){
    this.lPartidosCamaraByDept = this.partidosCamaraData[event.target.value];
    this.lCandidatosCamaraByDept = this.candidatosCamaraData[event.target.value];
    this.deptoValue = event.target.value;

    this.addPartDepItems = 9;
    this.addPartIndItems = 6;
    this.addPartAfrItems = 6;

    this.addCandDepItems = 9;
    this.addCandIndItems = 6;
    this.addCandAfrItems = 6;
  }


  addMorePartDepItems(){
    this.addPartDepItems += 9;
  }

  addMorePartIndItems(){
    this.addPartIndItems += 6;
  }

  addMorePartAfrItems(){
    this.addPartAfrItems += 6;
  }

  addMoreCandDepItems(){
    this.addCandDepItems += 9;
  }

  addMoreCandIndItems(){
    this.addCandIndItems += 6;
  }

  addMoreCandAfrItems(){
    this.addCandAfrItems += 6;
  }

}
