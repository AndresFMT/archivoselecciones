import { Component, Input, SimpleChange } from '@angular/core';

@Component({
  selector: 'app-consultas',
  templateUrl: './consultas.component.html',
  styleUrls: ['./consultas.component.sass']
})
export class ConsultasComponent {

  @Input() dataCIS;
  @Input() dataGCC;

  dataTotalInfo;
  lCandCIS;
  lCandGCC;
  print:boolean = false;  

  constructor() { }

  ngOnChanges(changes:SimpleChange){

    if(Object.keys(this.dataCIS).length && Object.keys(this.dataGCC).length){
      
      this.lCandCIS = this.dataCIS.candidatos;
      this.lCandGCC = this.dataGCC.candidatos;
      this.dataTotalInfo = this.dataCIS;
     
      this.print = true;  
    }

  }

}
