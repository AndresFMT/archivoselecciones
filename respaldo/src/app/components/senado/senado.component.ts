import { Component, Input, SimpleChange } from '@angular/core';
import { VotacionesSenModel } from '../../models/senado/votacionesSen.model';

@Component({
  selector: 'app-senado',
  templateUrl: './senado.component.html',
  styleUrls: ['./senado.component.sass']
})
export class SenadoComponent {

  @Input() dataSenado;
  dataSenadoFromPrincipal:VotacionesSenModel;
  dataSenNacional:VotacionesSenModel;
  dataSenIndigenas:VotacionesSenModel;
  seccionSen:string ='candSenSecc';
  itemsSenNacional:number;
  itemsSenIndigenas:number;
  addCanNacItems:number = 9;
  addCanIndItems:number = 6;
  addPartNacItems:number = 9;
  addPartIndItems:number = 6;
  print:boolean = false; 
  
  constructor() {}

  ngOnChanges(changes:SimpleChange){

    if(Object.keys(this.dataSenado).length){

      this.dataSenadoFromPrincipal = this.dataSenado;
      this.dataSenNacional = this.dataSenado.circunscripciones[0];
      this.dataSenIndigenas = this.dataSenado.circunscripciones[1];
      this.itemsSenNacional = this.dataSenado.circunscripciones[0].candidatos.length;
      this.itemsSenIndigenas = this.dataSenado.circunscripciones[1].candidatos.length;

      this.print = true;

    }

  }

  addMoreCanNacItems(){
    this.addCanNacItems += 9;
  }

  addMoreCanIndItems(){
    this.addCanIndItems += 6;
  }


  addMorePartNacItems(){
    this.addPartNacItems += 9;
  }

  addMorePartIndItems(){
    this.addPartIndItems += 6;
  }

  

}
