export class CandidatoCamModel {
    public id: number;
    public partidosNombrePartido: string; 
    public partidosCodigoPartidoPolitico: number;
    public candidatosNombre: string;
    public DetalleCandidatoVotos:  number;
    public DetalleCandidatoPorcentaje: number;
}