import { CandidatoCamModel } from '../candidatos/candidatoCam.model'

export class GruposCamDepartamentosModel {
    public corp: string;
    public votos_otros: number;
    public info: CandidatoCamModel[];
}