import { PartidoCamModel } from '../partidos/partidoCam.model'

export class GruposCamDepartamentosModel {
    public corp: string;
    public votos_otros: number;
    public info: PartidoCamModel[];
}