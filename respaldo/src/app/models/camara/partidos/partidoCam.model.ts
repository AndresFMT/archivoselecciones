export class PartidoCamModel {
    public id: number;
    public partidosNombrePartido: string;
    public DetallePartidoVotos: number;
    public DetallePartidoPorcentaje: number;
    public DetallePartidoPosiblesCurules: number;
}