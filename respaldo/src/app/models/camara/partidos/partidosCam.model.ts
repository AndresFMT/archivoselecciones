import { GruposCamDepartamentosModel } from '../partidos/gruposCam.departamento.model';
export class PartidosCamModel {
    public mesas_informadas: string;
    public mesas_instaladas: string;
    public hora: string;
    public fecha: string;
    public votos_nulos: string;
    public numero: number;
    public porcentaje_mesas_informadas: string;
    public departamento: string;
    public id: number;
    public data: GruposCamDepartamentosModel[];
}