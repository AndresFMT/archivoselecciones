import { PartidosCamModel } from './partidos/partidosCam.model';
import { CandidatosCamModel } from './candidatos/candidatosCam.model';
export class VotacionesCamModel {
    public partidos: PartidosCamModel[];
    public candidatos: CandidatosCamModel[];
}
