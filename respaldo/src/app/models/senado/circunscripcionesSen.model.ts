import { partidosSenModel } from './partidosSen.model' 
import { candidatosSenModel } from './candidatosSen.model' 

export class CircunscripcionesSenModel {
    public nombre:string;
    numero_curules:number;
    partido:partidosSenModel[];
    candidatos:candidatosSenModel[];
}