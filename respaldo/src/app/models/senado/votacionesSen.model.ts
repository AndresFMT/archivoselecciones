import { CircunscripcionesSenModel } from './circunscripcionesSen.model';

export class VotacionesSenModel {
    public mesas_informadas:string;
    public mesas_instaladas:string;
    public hora:string;
    public fecha:string;
    public votos_nulos:string;
    public numero:number;
    public porcentaje_mesas_informadas:string;
    public circuscripciones:CircunscripcionesSenModel[];
}
