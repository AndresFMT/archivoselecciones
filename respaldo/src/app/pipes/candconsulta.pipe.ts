import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'candconsulta'
})
export class CandconsultaPipe implements PipeTransform {

  transform(value:string, args: any) {
    if(value){
      let candidato = value;
      
      switch(candidato){
        case 'GUSTAVO PETRO':
          candidato = '1.jpg';
          break;
        case 'MARTHA LUCÍA RAMÍREZ':
          candidato = '2.jpg';
          break;
        case 'CARLOS CAICEDO':
          candidato = '3.jpg';
          break;
        case 'ALEJANDRO ORDOÑEZ':
          candidato = '4.jpg';
          break;
        case 'IVÁN DUQUE':
          candidato = '5.jpg';
          break;
        default: 
          candidato = 'default.jpg'
        
      }

      return candidato;
    }
  }

}
