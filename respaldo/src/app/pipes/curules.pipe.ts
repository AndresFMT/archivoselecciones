import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'curules'
})
export class CurulesPipe implements PipeTransform {

  transform(value:any, args:any){

    if(value){
      let curules = value;
      
      switch(curules){
        case 'AMAZONAS':
          curules = '1';
          break;
        case 'ANTIOQUIA':
          curules = '2';
          break;
        case 'ARAUCA':
          curules = '3';
          break;
        case 'ATLANTICO':
          curules = '4';
          break;
        case 'BOGOTA D.C.':
          curules = '5';
          break;
        case 'BOLIVAR':
          curules = '6';
          break;
        case 'BOYACA':
          curules = '7';
          break;
        case 'CALDAS':
          curules = '8';
          break;
        case 'CAQUETA':
          curules = '9';
          break;
        case 'CASANARE':
          curules = '10';
          break;
        case 'CAUCA':
          curules = '11';
          break;
        case 'CESAR':
          curules = '12';
          break;
        case 'CHOCO':
          curules = '13';
          break;
        case 'CORDOBA':
          curules = '14';
          break;
        case 'CUNDINAMARCA':
          curules = '15';
          break;
        case 'GUAINIA':
          curules = '16';
          break;
        case 'GUAVIARE':
          curules = '17';
          break;
        case 'HUILA':
          curules = '18';
          break;
        case 'LA GUAJIRA':
          curules = '19';
          break;
        case 'MAGDALENA':
          curules = '20';
          break;
        case 'META':
          curules = '21';
          break;
        case 'NARIÑO':
          curules = '22';
          break;
        case 'NORTE DE SAN':
          curules = '23';
          break;
        case 'PUTUMAYO':
          curules = '24';
          break;
        case 'QUINDIO':
          curules = '25';
          break;
        case 'RISARALDA':
          curules = '26';
          break;
        case 'SAN ANDRES':
          curules = '27';
          break;
        case 'SANTANDER':
          curules = '28';
          break;
        case 'SUCRE':
          curules = '29';
          break;
        case 'TOLIMA':
          curules = '30';
          break;
        case 'VALLE':
          curules = '31';
          break;
        case 'VAUPES':
          curules = '32';
          break;
        case 'VICHADA':
          curules = '33';
          break;
        default: 
          curules = 'N/A'
      }
      return curules;
    }
  }

}
