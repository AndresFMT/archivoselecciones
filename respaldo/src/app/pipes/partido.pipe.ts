import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'partido'
})
export class PartidoPipe implements PipeTransform {

  transform(value:string, args: any) {
    if(value){
      let partido = value;
      
      switch(partido){
        case 'PARTIDO CENTRO DEMOCRÁTICO':
          partido = '1.jpg';
          break;
        case 'PARTIDO LIBERAL COLOMBIANO':
          partido = '2.jpg';
          break;
        case 'PARTIDO ALIANZA VERDE':
          partido = '3.jpg';
          break;
        case 'PARTIDO SOCIAL DE UNIDAD NACIONAL PARTIDO DE LA U':
          partido = '4.jpg';
          break;
        case 'PARTIDO POLO DEMOCRÁTICO ALTERNATIVO':
          partido = '5.jpg';
          break;
        case 'PARTIDO CAMBIO RADICAL':
          partido = '6.jpg';
          break;
        case 'PARTIDO POLÍTICO MIRA':
          partido = '7.jpg';
          break;
        case 'PARTIDO CONSERVADOR COLOMBIANO':
          partido = '8.jpg';
          break;
        case 'PARTIDO UNIÓN PATRIÓTICA':
          partido = '9.jpg';
          break;
        case 'PARTIDO OPCIÓN CIUDADANA':
          partido = '10.jpg';
          break;
        case 'PARTIDO FUERZA ALTERNATIVA REVOLUCIONARIA DEL COMÚN':
          partido = '11.jpg';
          break;
        case 'G.S.C. COLOMBIA JUSTA LIBRES':
          partido = '12.jpg';
          break;
        case 'COALICIÓN LISTA DE LA DECENCIA (ASI,UP,MAIS)':
          partido = '13.jpg';
          break;
        case 'SI SE PUEDE':
          partido = '14.jpg';
          break;
        case 'PARTIDO SOMOS':
          partido = '15.jpg';
          break;
        case 'MOVIMIENTO TODOS SOMOS COLOMBIA \"TSC\"':
          partido = '16.jpg';
          break;
        default: 
          partido = 'default.jpg'
        
      }

      return partido;
    }
  }

}
