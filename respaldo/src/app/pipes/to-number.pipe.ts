import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'toNumber'
})
export class ToNumberPipe implements PipeTransform {

  transform(value:any, args: any){
    

    if(value){

      let number:any;
      let newNumber:any;

      number = value.split("%").join("");

      newNumber = parseFloat(number.split(",").join("."));

      return newNumber;

    }
  }

}
