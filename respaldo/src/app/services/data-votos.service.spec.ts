import { TestBed, inject } from '@angular/core/testing';

import { DataVotosService } from './data-votos.service';

describe('DataVotosService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DataVotosService]
    });
  });

  it('should be created', inject([DataVotosService], (service: DataVotosService) => {
    expect(service).toBeTruthy();
  }));
});
