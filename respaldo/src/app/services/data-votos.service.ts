import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';

@Injectable()
export class DataVotosService {
  // url:string = 'http://visor-js.herokuapp.com/http://noticiascaracol.s3.amazonaws.com/Votaciones2018/';
  url:string = 'https://elecciones.caracoltv.com/data-json/';
  // url:string = 'assets/jsonServices/';
  constructor(private dataVotosService:Http) {}

  getService(jsonUrl):Observable<any>{
    let stringUrl=this.url+jsonUrl;
    return this.dataVotosService.get(stringUrl)
      .map((res: any) => {
        return res.json();
      })

  }

}
